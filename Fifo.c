/******************************************************************************
 * Fifo.c
 *
 * Modification History:
 *
 * 01/29/2016 � Tom Kerr
 * Created.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Implements a FIFO - a byte queue with first in - first out access.
 *
 * Useful for buffering data streams such as UART receive and transmit.
 *
 * Designed for small, memory-limited 8-bit microcontroller platforms.
 * FIFO size is limited to 255 bytes.
 */
 
/******************************************************************************
 * Lint options.
 ******************************************************************************/
//lint -esym(714, FIFO_*)  Ignore 'not referenced'
//lint -esym(759, FIFO_*)  Ignore 'could be moved from header to module'
//lint -esym(765, FIFO_*)  Ignore 'could be made static'


/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "Fifo.h"

/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

 
/**************************************
 * FIFO_Define
 **************************************/
void FIFO_Define(FIFO* fifo, uint8_t* buffer, uint8_t size)
{
    /* head and tail point to base of buffer, set up end marker */
    fifo->buffer = buffer;
    fifo->head = buffer;
    fifo->tail = buffer;
    fifo->end = buffer + size;
    fifo->count = 0;
    fifo->size = size;
}

/**************************************
 * FIFO_QueryCount
 **************************************/
uint8_t FIFO_QueryCount(const FIFO* fifo)
{
    return fifo->count;
}

/**************************************
 * FIFO_QuerySpace
 **************************************/
uint8_t  FIFO_QuerySpace(const FIFO* fifo)
{
    return fifo->size - fifo->count;
}

/**************************************
 * FIFO_Add
 **************************************/
uint8_t  FIFO_Add(FIFO* fifo, const uint8_t* source, uint8_t count)
{
    uint8_t numadded;

    /* limit the number to add to the available space */
    if (count > FIFO_QuerySpace(fifo))
    {
        count = FIFO_QuerySpace(fifo);
    }
    numadded = count;
    fifo->count += count;
    for (; count > 0; count--)
    {
        *fifo->tail++ = *source++;       /* enqueue the byte */
        if (fifo->tail == fifo->end)     /* update tail with wrap */
        {
            fifo->tail = fifo->buffer;
        }
    }
    return numadded;
}

/**************************************
 * FIFO_Remove
 **************************************/
uint8_t  FIFO_Remove(FIFO* fifo, uint8_t* dest, uint8_t max)
{
    uint8_t numremoved;

    /* limit the number removed to the number in the fifo */
    if (max > FIFO_QueryCount(fifo))
    {
        max = FIFO_QueryCount(fifo);
    }

    numremoved = max;
    fifo->count -= numremoved;
    for (; max > 0; max--)
    {
        *dest++ = *fifo->head++;         /* dequeue the byte */
        if (fifo->head == fifo->end)     /* update head with wrap */
        {
            fifo->head = fifo->buffer;
        }
    }
    return numremoved;
}

/**************************************
 * FIFO_Clear
 **************************************/
void  FIFO_Clear(FIFO* fifo)
{
    fifo->count = 0;
    /* head points to tail, doesn't matter where in buffer */
    fifo->tail = fifo->head = fifo->buffer;
}

/**************************************
 * FIFO_Peek
 **************************************/
uint8_t  FIFO_Peek(const FIFO* fifo, uint8_t* dest, uint8_t max)
{
    uint8_t numretrieved;
    uint8_t* localHead = fifo->head;  // Do not modify FIFO pointers

    // Limit the number retrieved to the number in the fifo.
    if (max > FIFO_QueryCount(fifo))
    {
        max = FIFO_QueryCount(fifo);
    }

    numretrieved = max;
    
    for (; max > 0; max--)
    {
        *dest++ = *localHead++;         // retrieve the byte
        if (localHead == fifo->end)     // update local head with wrap
        {
            localHead = fifo->buffer;
        }
    }
    return numretrieved;
}


/**************************************
 * FIFO_Contains
 **************************************/
bool FIFO_Contains(const FIFO* fifo, uint8_t val)
{
    bool     found = false;
    uint8_t* localHead = fifo->head;          // Do not modify FIFO
    uint8_t  localCount = fifo->count;        // ...

    for (; localCount > 0; localCount--)
    {
        if (*localHead++ == val)
        {
            found = true;
            break;
        }
        if (localHead == fifo->end)
        {
            localHead = fifo->buffer;
        }
    }

    return found;
}


/**************************************
 * FIFO_Count
 **************************************/
uint8_t FIFO_Count(const FIFO* fifo, uint8_t val)
{
    uint8_t  matches = 0;
    uint8_t* localHead = fifo->head;          // Do not modify FIFO
    uint8_t  localCount = fifo->count;        // ...
    
    for (; localCount > 0; localCount--)
    {
        if (*localHead++ == val)
        {
            matches++;   
        }
        if (localHead == fifo->end)
        {
            localHead = fifo->buffer;
        }
    }
    
    return matches;
}


/**************************************
 * FIFO_Size
 **************************************/
uint8_t FIFO_Size(const FIFO* fifo)
{
    return fifo->size;
}

// End of file.

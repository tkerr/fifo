/******************************************************************************
 * Fifo.h
 *
 * Modification History:
 *
 * 01/29/2016 � Tom Kerr
 * Created.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Implements a FIFO - a byte queue with first in - first out access.
 *
 * Useful for buffering data streams such as UART receive and transmit.
 *
 * Designed for small, memory-limited 8-bit microcontroller platforms.
 * FIFO size is limited to 255 bytes.
 */

#ifndef _FIFO_H
#define _FIFO_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
* Public definitions.
******************************************************************************/

typedef struct _FIFO
{
    uint8_t* buffer;                 /* base of queue memory */
    uint8_t* head;                   /* current head of queue */
    uint8_t* tail;                   /* current tail of queue */
    uint8_t* end;                    /* end of queue memory */
    uint8_t  size;                   /* # of cells in queue */
    uint8_t  count;                  /* # of bytes in queue */
} FIFO;


/******************************************************************************
 * Public functions.
 ******************************************************************************/

#ifdef __cplusplus
    extern "C" {
#endif

/**
 * @brief
 * Prepares a FIFO structure by initializing the supplied buffer as an empty FIFO.
 *
 * Further references to the FIFO are made through the fifo structure.
 *
 * FIFO size is limited to 255 bytes.
 *
 * @param fifo Pointer to the FIFO structure.
 * @param buffer The buffer to use for the FIFO.
 * @param size The size of the buffer (and hence, the size of the FIFO).
 */
void FIFO_Define(FIFO* fifo, uint8_t* buffer, uint8_t size);

/**
 * @brief
 * Empties the FIFO by clearing the count and reseting the head and tail pointers.
 *
 * @param fifo Pointer to the FIFO.
 */
void FIFO_Clear(FIFO* fifo);

/**
 * @brief
 * Returns the number of bytes in the FIFO.
 *
 * @param fifo Pointer to the FIFO.
 * @return The number of bytes in the FIFO.
 */
uint8_t FIFO_QueryCount(const FIFO* fifo);

/**
 * @brief
 * Returns the number of empty bytes in the FIFO.
 *
 * @param fifo Pointer to the FIFO.
 * @return The number of empty bytes in the FIFO.
 */
uint8_t FIFO_QuerySpace(const FIFO* fifo);

/**
 * @brief
 * Adds bytes to the FIFO at the tail.
 *
 * Bytes are pulled from the source.
 * Count bytes are added to the FIFO as long as the fifo can hold
 * all of the data.
 *
 * @param fifo Pointer to the FIFO.
 * @param source Pointer to source data.
 * @param count The number of bytes to add.
 * @return The number of bytes added to the FIFO.
 */
uint8_t FIFO_Add(FIFO* fifo, const uint8_t* source, uint8_t count);

/**
 * @brief
 * Removes up to max bytes from the FIFO, placing the bytes at
 * the memory pointed to by the destination.
 *
 * @param fifo Pointer to the FIFO.
 * @param dest Destination pointer.
 * @param max The maximum number of bytes to remove from the FIFO.
 * @return The number of bytes removed from the FIFO.
 */
uint8_t FIFO_Remove(FIFO* fifo, uint8_t* dest, uint8_t max);
    
/**
 * @brief
 * Retrieves up to max bytes from the FIFO, placing the bytes at
 * the memory pointed to by the destination.  The bytes are not 
 * removed from the FIFO, and the FIFO pointers are not adjusted.
 *
 * @param fifo Pointer to the FIFO.
 * @param dest Destination pointer.
 * @param max The maximum number of bytes to retrieve from the FIFO.
 * @return The number of bytes retrieved from the FIFO.
 */
uint8_t FIFO_Peek(const FIFO* fifo, uint8_t* dest, uint8_t max);

/**
 * @brief
 * Determine if the FIFO contains the specified byte value.
 *
 * Scans the FIFO and looks for a byte that matches the
 * specified value.  The FIFO is not modified.
 *
 * @param fifo Pointer to the FIFO.
 * @param val The value to search.
 * @return True if val is found in the FIFO, false otherwise.
 */
bool FIFO_Contains(const FIFO* fifo, uint8_t val);

/**
 * @brief
 * Count the number of matching bytes in the FIFO.
 *
 * Scans the FIFO and returns the number of matching bytes.  The FIFO
 * is not modified.
 *
 * @param fifo Pointer to the FIFO.
 * @param val The value to search.
 * @return The number of bytes matching val.
 */
uint8_t FIFO_Count(const FIFO* fifo, uint8_t val);

/**
 * @brief
 * Return the FIFO size.
 */
uint8_t FIFO_Size(const FIFO* fifo);

#ifdef __cplusplus
    }
#endif

#endif /* FIFO_H */

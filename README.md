# FIFO #
FIFO Module.

Implements a FIFO - a byte queue with first in - first out access.

Useful for buffering data streams such as UART receive and transmit.

Designed for small, memory-limited 8-bit microcontroller platforms.
FIFO size is limited to 255 bytes.

### Documentation ###
Documentation is contained in the Fifo.h header file.

### Author ###
Tom Kerr AB3GY

### License ###
Released into the public domain.
For more information, please refer to https://unlicense.org
